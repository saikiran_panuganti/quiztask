//
//  Extensions.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation

extension NSDate {
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> NSDate {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT())
        return NSDate(timeIntervalSinceNow: seconds)
    }
}
