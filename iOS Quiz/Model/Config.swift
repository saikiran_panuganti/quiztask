//
//  Config.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation

class Config {
    static let shared = Config()
    
    var strings: Strings = Strings()
    var images:ImageNames = ImageNames()
}

class Strings {
    var submit = "Submit"
    var next = "Next"
    var previous = "Previous"
}

class ImageNames {
    var selectedImage = "radioButtonSelect"
    var unSelectedImage = "radioButtonUnSelect"
}
