//
//  Quiz.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation

struct Quiz: Codable {
    var quiz: [Question]

    enum CodingKeys: String, CodingKey {
        case quiz = "Quiz"
    }
}

struct Question: Codable {
    let question: String
    let mcqAnswer: [String]
    var selectedOption : Int?

    enum CodingKeys: String, CodingKey {
        case question = "Question"
        case mcqAnswer = "MCQ Answer"
    }
}

