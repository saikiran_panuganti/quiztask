//
//  ResultViewModel.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 26/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import UIKit
import CoreData

protocol ResultViewModelDelegate : class {
    func updateUI()
}

class ResultViewModel {
    
    weak var delegate : ResultViewModelDelegate?
    
    var results : [Result] = []
    
    func getData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Result")
        request.returnsObjectsAsFaults = false
        
        do {
            results = try context.fetch(request) as! [Result]
            results.sort {
                Float($0.date?.timeIntervalSinceReferenceDate ?? Date().timeIntervalSinceReferenceDate) > Float($1.date?.timeIntervalSinceReferenceDate ?? Date().timeIntervalSinceReferenceDate)
            }
            
            delegate?.updateUI()
        }catch {
            print(error.localizedDescription)
        }
    }
}

extension ResultViewModel : ResultViewDataSource {
    func numberOfRows() -> Int {
        return results.count
    }
    
    func sizeForItemAtIndex(_ index: Int) -> CGFloat {
        return 50.0
    }
    
    func dataSourceForCellAtIndex(_ index: Int) -> Result {
        return results[index]
    }
}
