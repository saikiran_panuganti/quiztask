//
//  QuizViewModel.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 26/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol QuizViewModelDelegate : class {
    func updateUI()
    func popTheController()
}

class QuizViewModel {
    weak var delegate : QuizViewModelDelegate?
    var quizData : Quiz?
    var currentQuestion : Int = 1
    var quizAnswers = [2, 3, 2, 3, 2]
    
    func getDataFromFile() {
        if let path = Bundle.main.url(forResource: "Data", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                quizData = try JSONDecoder().decode(Quiz.self, from: data)
                
                delegate?.updateUI()
            }catch {
                print(error.localizedDescription)
            }
        }else {
            print("Invalid File Path")
        }
    }
    
    func evaluateAndSaveResult() {
        var score : Int = 0
        
        if let questions = quizData?.quiz {
            for i in 0..<questions.count {
                if questions[i].selectedOption == quizAnswers[i] {
                    score += 5
                }else {
                    score -= 2
                }
            }
            
            print("Your Score is ", score)
            saveResultToCoreData(score: score)
        }
    }
    
    func saveResultToCoreData(score : Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let resultEntity = NSEntityDescription.entity(forEntityName: "Result", in: context)!
        let questionEntity = NSEntityDescription.entity(forEntityName: "Questions", in: context)!
        
        let result = NSManagedObject(entity: resultEntity, insertInto: context)
        let question = NSManagedObject(entity: questionEntity, insertInto: context)
        
        question.setValue(quizData?.quiz[0].selectedOption ?? 10, forKey: "question1")
        question.setValue(quizData?.quiz[1].selectedOption ?? 10, forKey: "question2")
        question.setValue(quizData?.quiz[2].selectedOption ?? 10, forKey: "question3")
        question.setValue(quizData?.quiz[3].selectedOption ?? 10, forKey: "question4")
        question.setValue(quizData?.quiz[4].selectedOption ?? 10, forKey: "question5")
        
        result.setValue(NSDate(), forKey: "date")
        result.setValue(score, forKey: "score")
        result.setValue(question, forKey: "answers")
        
        do{
            try context.save()
            
            delegate?.popTheController()
        }
        catch{
            print(error.localizedDescription)
        }
    }
    
    func nextTapped() {
        if quizData?.quiz[currentQuestion-1].selectedOption != nil {
            if currentQuestion < (quizData?.quiz.count ?? 1) {
                currentQuestion += 1
                delegate?.updateUI()
            }else if currentQuestion == (quizData?.quiz.count ?? 1) {
                evaluateAndSaveResult()
            }
        }
    }
    
    func previousTapped() {
        if currentQuestion > 1 {
            currentQuestion -= 1
            delegate?.updateUI()
        }
    }
    
    func option1Selected() {
        quizData?.quiz[currentQuestion-1].selectedOption = 1
        delegate?.updateUI()
    }
    
    func option2Selected() {
        quizData?.quiz[currentQuestion-1].selectedOption = 2
        delegate?.updateUI()
    }
    
    func option3Selected() {
        quizData?.quiz[currentQuestion-1].selectedOption = 3
        delegate?.updateUI()
    }
    
    func option4Selected() {
        quizData?.quiz[currentQuestion-1].selectedOption = 4
        delegate?.updateUI()
    }
}

extension QuizViewModel : QuizViewDataSource {
    var question: Question? {
        return quizData?.quiz[currentQuestion-1]
    }
    
    var showPreviousButton: Bool {
        return currentQuestion == 1 ? false : true
    }
    
    var showNextButton: Bool {
        return currentQuestion == quizData?.quiz.count ? false : true
    }
}
