//
//  QuizViewController.swift
//  iOS Quiz
//
//  Created by Jitender Kumar Yadav on 27/10/20.
//  Copyright © 2020 Jitender Kumar Yadav. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {
    
    var viewModel : QuizViewModel = QuizViewModel()
    @IBOutlet weak var quizView : QuizView!

    override func viewDidLoad() {
        super.viewDidLoad()

        quizView.delegate = self
        viewModel.delegate = self
        viewModel.getDataFromFile()
    }

}

extension QuizViewController : QuizViewModelDelegate {
    func updateUI() {
        quizView.dataSource = viewModel
        quizView.updateUI()
    }
    
    func popTheController() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension QuizViewController : QuizViewDelegate {
    func nextTapped() {
        viewModel.nextTapped()
    }
    
    func previousTapped() {
        viewModel.previousTapped()
    }
    
    func option1Selected() {
        viewModel.option1Selected()
    }
    
    func option2Selected() {
        viewModel.option2Selected()
    }
    
    func option3Selected() {
        viewModel.option3Selected()
    }
    
    func option4Selected() {
        viewModel.option4Selected()
    }
}
