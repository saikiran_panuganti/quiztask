//
//  ResultViewController.swift
//  iOS Quiz
//
//  Created by Jitender Kumar Yadav on 27/10/20.
//  Copyright © 2020 Jitender Kumar Yadav. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var resultView : ResultView!
    var viewModel : ResultViewModel = ResultViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultView.delegate = self
        viewModel.delegate = self
        resultView.setUpUI()
        viewModel.getData()
    }

}

extension ResultViewController : ResultViewModelDelegate {
    func updateUI() {
        resultView.dataSource = viewModel
        resultView.updateUI()
    }
}

extension ResultViewController : ResultViewDelegate {
    
}
