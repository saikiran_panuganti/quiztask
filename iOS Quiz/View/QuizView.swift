//
//  QuizView.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import UIKit

protocol QuizViewDataSource : class {
    var question : Question? {get}
    var showPreviousButton : Bool {get}
    var showNextButton : Bool {get}
}

protocol QuizViewDelegate : class {
    func nextTapped()
    func previousTapped()
    func option1Selected()
    func option2Selected()
    func option3Selected()
    func option4Selected()
}

class QuizView : UIView {
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var option1 : UILabel!
    @IBOutlet weak var option2 : UILabel!
    @IBOutlet weak var option3 : UILabel!
    @IBOutlet weak var option4 : UILabel!
    @IBOutlet weak var image1 : UIImageView!
    @IBOutlet weak var image2 : UIImageView!
    @IBOutlet weak var image3 : UIImageView!
    @IBOutlet weak var image4 : UIImageView!
    @IBOutlet weak var previousButton : UIButton!
    @IBOutlet weak var nextButton : UIButton!
    
    weak var dataSource : QuizViewDataSource?
    weak var delegate : QuizViewDelegate?
    
    func updateUI() {
        question.text = dataSource?.question?.question ?? ""
        option1.text = dataSource?.question?.mcqAnswer[0] ?? ""
        option2.text = dataSource?.question?.mcqAnswer[1] ?? ""
        option3.text = dataSource?.question?.mcqAnswer[2] ?? ""
        option4.text = dataSource?.question?.mcqAnswer[3] ?? ""
        
        previousButton.setTitle(Config.shared.strings.previous, for: .normal)
        
        updatUserSelection(option : dataSource?.question?.selectedOption)
        
        if !(dataSource?.showPreviousButton ?? true) {
            previousButton.isHidden = true
        }else {
            previousButton.isHidden = false
        }
        
        if !(dataSource?.showNextButton ?? true) {
            nextButton.setTitle(Config.shared.strings.submit, for: .normal)
        }else {
            nextButton.setTitle(Config.shared.strings.next, for: .normal)
        }
    }
    
    func updatUserSelection(option : Int?) {
        switch option {
        case 1:
            image1.image = UIImage.init(named: Config.shared.images.selectedImage)
            image2.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image3.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image4.image = UIImage.init(named: Config.shared.images.unSelectedImage)
        case 2:
            image1.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image2.image = UIImage.init(named: Config.shared.images.selectedImage)
            image3.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image4.image = UIImage.init(named: Config.shared.images.unSelectedImage)
        case 3:
            image1.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image2.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image3.image = UIImage.init(named: Config.shared.images.selectedImage)
            image4.image = UIImage.init(named: Config.shared.images.unSelectedImage)
        case 4:
            image1.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image2.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image3.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image4.image = UIImage.init(named: Config.shared.images.selectedImage)
        default:
            image1.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image2.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image3.image = UIImage.init(named: Config.shared.images.unSelectedImage)
            image4.image = UIImage.init(named: Config.shared.images.unSelectedImage)
        }
    }
    
    @IBAction func nextTapped(sender : UIButton) {
        delegate?.nextTapped()
    }
    
    @IBAction func previousTapped(sender : UIButton) {
        delegate?.previousTapped()
    }
    
    @IBAction func option1Selected() {
        delegate?.option1Selected()
    }
    
    @IBAction func option2Selected() {
        delegate?.option2Selected()
    }
    
    @IBAction func option3Selected() {
        delegate?.option3Selected()
    }
    
    @IBAction func option4Selected() {
        delegate?.option4Selected()
    }
}
