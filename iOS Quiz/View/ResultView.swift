//
//  ResultView.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import UIKit

protocol ResultViewDataSource : class {
    func numberOfRows() -> Int
    func sizeForItemAtIndex(_ index: Int) -> CGFloat
    func dataSourceForCellAtIndex(_ index: Int) -> Result
}

protocol ResultViewDelegate : class {
    
}

class ResultView : UIView {
    
    @IBOutlet weak var tableView : UITableView!
    
    weak var dataSource : ResultViewDataSource?
    weak var delegate : ResultViewDelegate?
    
    func updateUI() {
        tableView.reloadData()
    }
    
    func setUpUI() {
        tableView.register(UINib(nibName: "ResultTableViewCell", bundle: nil), forCellReuseIdentifier: "ResultTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension ResultView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell", for: indexPath) as! ResultTableViewCell
        cell.updateData(result: dataSource?.dataSourceForCellAtIndex(indexPath.row))
        
        return cell
    }
}

extension ResultView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataSource?.sizeForItemAtIndex(indexPath.row) ?? 50.0
    }
}


