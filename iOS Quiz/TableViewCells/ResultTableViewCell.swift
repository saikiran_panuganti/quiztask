//
//  ResultTableViewCell.swift
//  iOS Quiz
//
//  Created by SaiKiran Panuganti on 27/03/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {

    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var score : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    func updateData(result : Result?) {
        if let result = result {
            date.text = Formatters.shared.getStringFromDate(result.date, dateFormatType: .YYYYMMDDTHHmmSS, requiredFormatType: .ddMMMYYYYmmSS)
            score.text = String(result.score)
        }
    }
    
}
