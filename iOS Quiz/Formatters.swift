//
//  Formatters.swift
//  Talkies
//
//  Created by santhoshkumar nampally on 23/03/20.
//  Copyright © 2020 santhoshkumar nampally. All rights reserved.
//

import Foundation

enum FormatType: String {
    case YYYYMMDDTHHmmSS = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case ddMMMYYYYmmSS = "dd MMM yyyy hh:mm a"
}

class Formatters {
    static let shared: Formatters = Formatters()
    
    private let dateFormater = DateFormatter()
    
    func getTheTransformedString(_ dataStr:String, dateFormatType:FormatType, requiredFormatType:FormatType) -> String {
        dateFormater.dateFormat = dateFormatType.rawValue
        
        if let date = dateFormater.date(from: dataStr){
            dateFormater.dateFormat = requiredFormatType.rawValue
            return dateFormater.string(from: date)
        }
        
        return ""
    }
    
    func getStringFromDate(_ date:Date?, dateFormatType:FormatType, requiredFormatType:FormatType) -> String
    {
        dateFormater.dateFormat = dateFormatType.rawValue
        
        if let date = date {
            let dateStr = dateFormater.string(from: date)
            dateFormater.dateFormat = requiredFormatType.rawValue
            return getTheTransformedString(dateStr, dateFormatType: dateFormatType, requiredFormatType: requiredFormatType)
        }
        
        return ""
    }
}
